const messages = document.getElementById("messages");
let numMsg = Number(localStorage.numMsg);

function fillMessages() {
    for (let k=0; k<numMsg; ++k){
        const x = document.createElement("li");
        messages.appendChild(x);
        messages.lastChild.setAttribute("class", "list-group-item text-left");
        messages.lastChild.innerHTML = localStorage.getItem("message" + (numMsg - k -1));
    }
}