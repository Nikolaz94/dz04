if(!localStorage.message3) {
    localStorage.setItem("message0", "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Totam sunt culpa iure reprehenderit? Repellendus neque quisquam odit, culpa numquam laudantium dolore rem molestias commodi dicta.");
    localStorage.setItem("message1", "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Repellendus non nobis fugit ea illum blanditiis pariatur deserunt maxime temporibus illo.");
    localStorage.setItem("message2", "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illo, minima neque. Ea, asperiores. Facilis, voluptates?");
    localStorage.setItem("numMsg", 3);
    localStorage.setItem("coverNum", 0);
    localStorage.setItem("profileNum", 0);
}


const userName = document.getElementById("userName");
const email = document.getElementById("email");
const messages = document.getElementById("messages");

function fillData() {
    userName.innerHTML = localStorage.UserName;
    email.innerHTML = localStorage.Email;

    updateMsgs()
}

const messageInput = document.getElementById("messageInput");

function countChars() {
    let n = messageInput.value.length; 
    if (n < 501) {
        document.getElementById("emailHelp").innerHTML = (500 - n) + " characters left";
        document.getElementById("submitBtn").removeAttribute("disabled");
    } else {
        document.getElementById("emailHelp").innerHTML = "Message too long - Unable to submit";
        document.getElementById("submitBtn").setAttribute("disabled", "disabled");
    }
}

function submitMsg() {
    if (messageInput.value != ""){
        localStorage.setItem("message" + (localStorage.numMsg), messageInput.value);
        messageInput.value = "";
        localStorage.numMsg = Number(localStorage.numMsg) + 1;
        updateMsgs()
    }
}

function updateMsgs() {
    let numMsg = Number(localStorage.numMsg);
    const max = Number(localStorage.numMsg);
    if (numMsg > 4) {
        numMsg = 5;
        document.getElementById("msgLink").removeAttribute("hidden");
    }

    if(messages.childNodes.length !== 0){
        for(let j=0; j<numMsg-1; ++j){
            let message = messages.firstChild;
            if(message != null) {
                message.remove();
            }
        }
    }
    
    for (let k=0; k<numMsg; ++k){
        const x = document.createElement("li");
        messages.appendChild(x);
        messages.lastChild.setAttribute("class", "list-group-item text-left");
        messages.lastChild.innerHTML = localStorage.getItem("message" + (max - k - 1));
    }
}

function changeImg(id) {
    const selectedImg = document.getElementById(id).files[0];
    const img =  document.getElementById(id + "Image");
    const reader  = new FileReader();
    reader.addEventListener("load", function () {
        img.src = reader.result;
        localStorage.setItem(id + "Num", Number(localStorage.getItem(id + "Num")) + 1);
        localStorage.setItem(id + localStorage.getItem(id + "Num"), reader.result);
    }, false);
    if (selectedImg) {
        reader.readAsDataURL(selectedImg);
    }
}