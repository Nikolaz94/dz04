const login = document.forms["LoginForm"];
function sendData() {
    if (typeof(Storage) !== "undefined") {
        const userName = document.forms["LoginForm"]["usrName"].value
        const email = document.forms["LoginForm"]["email"].value;
        const pass = document.forms["LoginForm"]["pass"].value;
        localStorage.setItem("UserName", userName);
        localStorage.setItem("Email", email);
        localStorage.setItem("Password", pass);
    } else {
        const parent = login.parentNode;
        const num = parent.childElementCount;
        for(let i = 0; i < 2*num; i++){
            parent.firstChild.remove();
        }
        const x = document.createElement("h1");
        parent.appendChild(x);
        parent.lastChild.setAttribute("class", "display-1");
        parent.lastChild.innerHTML = "Sorry! No Web Storage support.";
    }
}